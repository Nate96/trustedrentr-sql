drop schema if exists trustedrentr_amenity;

create schema trustedrentr_amenity;
use trustedrentr_amenity;

drop table if exists amenity;

create table amenity (
    amenity_id int not null auto_increment,
    unit_id int not null,
    amenity_name varchar(20)

    primary key(amenity_id)
) engine=MyISAM default charset=utf8;