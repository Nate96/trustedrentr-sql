drop schema if exists trustedrentr_property;

create schema trustedrentr_property;
use trustedrentr_property;

drop table if exists property; 

create table property (
    property_id int not null auto_increment,
    owner_id int not null,
    discription varchar(140) not null,
    property_street varchar(100) not null,
    property_city varchar(50) not null,
    property_state varchar(50) not null,
    property_zip int not null,
    has_multiple_units boolean not null

    primary key (property_id)
) engine=MyISAM default charset=utf8;

insert into property (owner_id, discription, property_street, property_city, property_state, property_zip, has_multiple_units) values (5, "discription", "123 street", "city", "state", 12345, true);
insert into property (owner_id, discription, property_street, property_city, property_state, property_zip, has_multiple_units) values (5, "discription", "321 street", "city", "state", 12345, false);
insert into property (owner_id, discription, property_street, property_city, property_state, property_zip, has_multiple_units) values (6, "discription", "213 street", "city", "state", 12346, true);