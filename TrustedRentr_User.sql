drop schema if exists trustedrentr_user;

create schema trustedrentr_user;
use trustedrentr_user;

drop table if exists user;

create table user (
    user_id int not null auto_increment,
    created_at datetime not null,
    updated_at datetime not null,
    rent_score int(4) default null, 
    first_name varchar(20) not null,
    last_name varchar(50) not null,
    display_name varchar(50) not null,
    email varchar(150) not null,
    email_verified boolean default false,
    user_role varchar(20) not null,
    phone_number varchar(11),
    unit_id int default null

    primary key (user_id)
) engine=MyISAM default charset=utf8; 

insert into user (created_at, updated_at, rent_score, first_name, last_name, display_name, email, user_role, phone_number) values (now(), now(), 500, "John", "Doe", "John Doe", "JohnD@email.com", "tenant", "1234567890");
insert into user (created_at, updated_at, rent_score, first_name, last_name, display_name, email, user_role, phone_number) values (now(), now(), 740, "Richard", "Bay", "Richard", "RichardB@email.com", "tenant", "1234567891");
insert into user (created_at, updated_at, rent_score, first_name, last_name, display_name, email, user_role, phone_number) values (now(), now(), 600, "Billay", "Bob", "Billaaayyy", "BillayB@email.com", "tenant", "1234567892"); 
insert into user (created_at, updated_at, rent_score, first_name, last_name, display_name, email, user_role, phone_number) values (now(), now(), 780, "Debby", "Deul", "Debby", "Debby@email.com", "tenant", "1234567893");
insert into user (created_at, updated_at, rent_score, first_name, last_name, display_name, email, user_role, phone_number) values (now(), now(), 0, "Lilly", "Alderan", "Lilly", "LillyA@email.com", "landlord", "1234567894"); 
insert into user (created_at, updated_at, rent_score, first_name, last_name, display_name, email, user_role, phone_number) values (now(), now(), 0, "Penny", "Bates", "Penny", "PennyB@email.com" "landlord", "1234567895");
