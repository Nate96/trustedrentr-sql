drop schema if exists trustedrentr_unit;

create schema trustedrentr_unit;
use trustedrentr_unit;

drop table if exists unit;

create table unit (
    unit_id int not null auto_increment,
    discription varchar(140),
    property_id int not null,
    unit_name varchar(20),
    bedrooms int not null,
    bathrooms int not null,
    accepting_applications boolean not null,
    move_in_date datetime not null,
    move_out_date datetime not null,
    square_feet int not null,

    primary key (unit_id)
) engine=MyISAM default charset=utf8;