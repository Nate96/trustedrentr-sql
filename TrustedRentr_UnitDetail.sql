drop schema if exists trustedrentr_unit_details;

create schema trustedrentr_unit_details;
use trustedrentr_unit_details;

drop table if exists unit_details;

create table unit_details (
    detail_id int not null auto_increment,
    unit_id int not null,
    detail_name varchar(20)

    primary key(detail_id)
) engine=MyISAM default charset=utf8;