drop schema if exists trustedrentr_applicant;

create schema trustedrentr_applicant;
use trustedrentr_applicant;

drop table if exists applicant;

create table applicant (
    applicantion_id int not null auto_increment,
    applicant_id int not null,
    landlord_id int not null,
    unit_id int not null,
    residency_type varchar(15),
    current_street varchar(100) not null,
    current_city varchar(50) not null,
    current_state varchar(50) not null,
    current_zip int not null,
    move_in_date datetime not null,
    move_out_date datetime not null,
    reason_leaving varchar(140),

    primary key(applicant_id)
) engine=MyISAM default charset=utf8;