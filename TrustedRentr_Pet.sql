drop schema if exists trustedrentr_pet;

create schema trustedrentr_pet;
use trustedrentr_pet;

drop table if exists pet;

create table pet (
    pet_id int not null auto_increment,
    unit_id int not null,
    pet_type varchar(15)

    primary key(pet_id)
) engine=MyISAM default charset=utf8;